! single/double reals
program real_kind
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: a
real(dp) :: b
a = 0
a = 1
a = 3.14
a = 3.14_sp
a = 3.14_dp

b = 0
b = 1
b = 3.14
b = 3.14_sp
b = 3.14_dp
end program
