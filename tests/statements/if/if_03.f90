! multi line if statement
program if_03
implicit none
integer :: i
i = 1
if (i == 1) then
    print *, "1"
end if
if (i == 2) then
    print *, "2"
end if
end
