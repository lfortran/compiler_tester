! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: x4, x4_ref
real(dp) :: x8, x8_ref
x4 = 1._sp
x4 = erf(x4)
x4 = erf(1._sp)
x4_ref = 0.8427007929497148_sp
if (abs(x4 - x4_ref) > 1e-5_sp) error stop "erf(1._sp)"

x8 = 1._dp
x8 = erf(x8)
x8 = erf(1._dp)
x8_ref = 0.8427007929497148_dp
if (abs(x8 - x8_ref) > 1e-10_dp) error stop "erf(1._dp)"
end program