! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: x4, x4_ref
real(dp) :: x8, x8_ref
x4 = real(2, sp)
x4_ref = 2._sp
if (abs(x4 - x4_ref) > 1e-5_sp) error stop "real(2, sp)"

x8 = real(2, dp)
x8_ref = 2._dp
if (abs(x8 - x8_ref) > 1e-10_dp) error stop "real(2, dp)"
end program
