! scalar single/double complex
program t02_scalar_complex
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: r4
real(dp) :: r8
complex(sp) :: x4
complex(dp) :: x8
x4 = (3.14_sp, 3._sp)
r4 = abs(x4)
r4 = abs((3.14_sp, 3._sp))

x8 = (3.14_dp, 3._dp)
r8 = abs(x8)
r8 = abs((3.14_dp, 3._dp))
end program
