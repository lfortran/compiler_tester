! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: x4, x4_ref
real(dp) :: x8, x8_ref
x4 = 1._sp
x4 = tanh(x4)
x4 = tanh(1._sp)
x4_ref = 0.7615941559557649_sp
if (abs(x4 - x4_ref) > 1e-5_sp) error stop "tanh(1._sp)"

x8 = 1._dp
x8 = tanh(x8)
x8 = tanh(1._dp)
x8_ref = 0.7615941559557649_dp
if (abs(x8 - x8_ref) > 1e-10_dp) error stop "tanh(1._dp)"
end program